function generateTable()
{
    if(!inputsValid())
    {
        alert("Number must be bigger than 0 and consist of two digits");
        return;
    } 
    checkForTable();
    var n = document.getElementById("nInput").value;
    var m = document.getElementById("mInput").value;
    var table = document.createElement("table");
    for(let i = 0 ; i < n; i++)
    {
        let tr = document.createElement("tr");

        for(let j = 0; j < m; j++)
        {
            let td = document.createElement("td");
            td.style.background = "white";
            let text = document.createTextNode((i+1).toString() + (j+1).toString());
           // td.addEventListener("click", changeColor);
            td.appendChild(text);
            tr.appendChild(td);
        }

        table.appendChild(tr);
    }
    table.onclick = function(event)
    {
        if (event.target.tagName != 'TD') return;
        if (checkForBorder(event))
        {
            alert("fool");
            return;
        }
        changeColor(event.target);
    }
    document.body.appendChild(table);
}
function inputsValid()
{
    let valid = true;
    let inputs = document.querySelectorAll(".inputs > input");
    inputs.forEach(input => {
        if(!input.checkValidity()) 
        {
            input.value="";
            valid = false;
        }
    });
    return valid;
}
function checkForTable()
{
    let table = document.getElementsByTagName("table")[0];
    if(table != null) table.remove(); 
}
function changeColor(target)
{
    if(target.style.background !== "white")
    {
        target.style.background = "white";
        return false;
    }
    target.style.background = getRandomColor();
}
function getRandomColor()
{
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function checkForBorder(clickPos)
{
    //var border_width = table.style.borderWidth;
    //alert(clickPos.target.clientHeight);
    let elem = clickPos.target;
    var border_width = (elem.offsetWidth - elem.clientWidth);
    //alert(border_width);
    //alert(getComputedStyle(clickPos.target, "border-width"));
    if(clickPos.offsetX < border_width || clickPos.offsetX > elem.clientWidth || clickPos.offsetY < border_width || clickPos.offsetY > elem.clientHeight){
        return true;
    }
    return false;
}